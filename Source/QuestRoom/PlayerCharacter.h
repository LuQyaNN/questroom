// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class QUESTROOM_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	APlayerCharacter();

	UClass* item;
	
	virtual void BeginPlay() override;
	
	
	virtual void Tick( float DeltaSeconds ) override;

	
	virtual void SetupPlayerInputComponent(class UInputComponent* ic) override;

	void MoveForward(float val);

	void MoveRight(float val);

	void LookUp(float val);

	void LookRight(float val);

	void FindItems();

	void SaveGame();
	void LoadGame();
	void PickUp();
	void Drop();

	TArray<TPair<float,class UInspectComponent*> > FindedItems;
	
	TArray<ETouchIndex::Type> Touches;

	

	void TouchPressed(ETouchIndex::Type finger, FVector pos);
	void TouchReleased(ETouchIndex::Type finger, FVector pos);
	void TouchDoublePressed(ETouchIndex::Type finger, FVector pos);

	

	void OnMouseWheelUp(float val);

	void TouchesUpdate(float delta);
	
	inline APlayerController* GetPC() { return Cast<APlayerController>(GetController()); }


	UPROPERTY(EditAnywhere)
		UCameraComponent* CameraComponent;

	UInspectComponent* CurrentInspectComponent;
	float mLastX = 0.f, mLastY = 0.f,LastDistZoom=0.f;

	UPROPERTY(BlueprintReadOnly)
	bool bInInspect = false;
	
	bool bTouchInRightZone = false;
private:
	
	FTimerHandle FindItemHandle;
	

	
	
};
