// Fill out your copyright notice in the Description page of Project Settings.

#include "QuestRoom.h"
#include "PlayerCharacter.h"
#include "Components/InspectComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MySaveGame.h"



// Sets default values
APlayerCharacter::APlayerCharacter() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->AttachToComponent(GetCapsuleComponent(),
		FAttachmentTransformRules::KeepRelativeTransform);
	CameraComponent->bUsePawnControlRotation = true;
	CameraComponent->AddWorldOffset(FVector(0, 0, 80));

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay() {
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FindItemHandle, this, &APlayerCharacter::FindItems,
		0.033f, true);
	GetPC()->SetInputMode(FInputModeGameAndUI());
}

void APlayerCharacter::TouchesUpdate(float delta) {
	if (Touches.Num() > 0) {
		if (Touches.Num() > 1) {
			if (bInInspect) {
				float mX1 = 0.f, mY1 = 0.f, mX2 = 0.f, mY2 = 0.f, deltaZoom = 0.f;
				bool isPressed = false;
				GetPC()->GetInputTouchState(Touches[0], mX1, mY1, isPressed);
				GetPC()->GetInputTouchState(Touches[1], mX2, mY2, isPressed);
				float dist = FVector2D::Distance(FVector2D(mX1, mY1), FVector2D(mX2, mY2));
				deltaZoom = dist - LastDistZoom;
				LastDistZoom = dist;
				OnMouseWheelUp(deltaZoom /20);
				return;
			}
		}
		float mX = 0.f, mY = 0.f, mDeltaX = 0.f, mDeltaY = 0.f;
		bool isPressed = false;

		FVector2D size;
		GEngine->GameViewport->GetViewportSize(size);

		GetPC()->GetInputTouchState(Touches[0], mX, mY, isPressed);




		mDeltaX = mX - mLastX;
		mLastX = mX;
		mDeltaY = mY - mLastY;
		mLastY = mY;
		if (bInInspect) {
			if (CurrentInspectComponent->IsValidLowLevel() &&
				CurrentInspectComponent->bReadyForInteract) {


				CurrentInspectComponent->Rotate(FQuat(CameraComponent->GetUpVector(),
					GetWorld()->GetDeltaSeconds()* -mDeltaX));
				CurrentInspectComponent->Rotate(FQuat(CameraComponent->GetRightVector(),
					GetWorld()->GetDeltaSeconds()* -mDeltaY));
			}
		}
		else {
			LookUp(-mDeltaY);
			LookRight(mDeltaX);
		}
	}
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	TouchesUpdate(DeltaTime);
	

}




// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* ic) {
	Super::SetupPlayerInputComponent(ic);
	ic->BindAxis(FName("MoveForward"), this, &APlayerCharacter::MoveForward);
	ic->BindAxis(FName("MoveRight"), this, &APlayerCharacter::MoveRight);
	ic->BindTouch(EInputEvent::IE_Pressed, this, &APlayerCharacter::TouchPressed);
	ic->BindTouch(EInputEvent::IE_Released, this, &APlayerCharacter::TouchReleased);
	ic->BindTouch(EInputEvent::IE_DoubleClick, this, &APlayerCharacter::TouchDoublePressed);
	ic->BindAction(FName("SaveGame"),EInputEvent::IE_Pressed, this, &APlayerCharacter::SaveGame);
	ic->BindAction(FName("LoadGame"), EInputEvent::IE_Pressed, this, &APlayerCharacter::LoadGame);
	ic->BindAction(FName("PickUp"), EInputEvent::IE_Pressed, this, &APlayerCharacter::PickUp);
	ic->BindAction(FName("Drop"), EInputEvent::IE_Pressed, this, &APlayerCharacter::Drop);
}

void APlayerCharacter::SaveGame() {
	
	UMySaveGame* SaveGameInstance = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	SaveGameInstance->item = item;
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, "1", 1);
}

void APlayerCharacter::LoadGame() {

	UMySaveGame* SaveGameInstance = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot("1", 1));
	item = SaveGameInstance->item;
}

void APlayerCharacter::PickUp()
{
	FHitResult hit;
	FCollisionQueryParams cqp(TEXT("FindItemsTrace"), false, this);
	UE_LOG(LogTemp, Warning, TEXT("1"));
	if (GetWorld()->LineTraceSingleByChannel(hit, CameraComponent->GetComponentLocation(), CameraComponent->GetComponentLocation()+CameraComponent->GetForwardVector()*200.f,
		ECollisionChannel::ECC_GameTraceChannel3,cqp)) {
		item = hit.GetActor()->StaticClass();
		hit.GetActor()->Destroy();
		UE_LOG(LogTemp, Warning, TEXT("2"));
	}
	

}

void APlayerCharacter::Drop()
{
	UE_LOG(LogTemp, Warning, TEXT("1"));
	FVector loc = CameraComponent->GetComponentLocation() + CameraComponent->GetForwardVector()*200.f;
	GetWorld()->SpawnActor(item, &loc);
}

void APlayerCharacter::MoveForward(float val) {
	if (val != 0) {
		if (!bInInspect) {
			GetMovementComponent()->AddInputVector(GetCapsuleComponent()->GetForwardVector()*
				val*GetWorld()->GetDeltaSeconds() * 100);
		}
		else {
			switch (CurrentInspectComponent->InspectType) {
			case EInspectType::ObjectToCam: {
				
					CurrentInspectComponent->Move(CameraComponent->GetUpVector()*val*50.f*
						GetWorld()->GetDeltaSeconds());

				
				
				break;
			}
			case EInspectType::CamToObject: {
				CurrentInspectComponent->Move(FVector(0, 0, val*50.f*
					GetWorld()->GetDeltaSeconds()));
				break;
			}
			}




		}

	}
}

void APlayerCharacter::MoveRight(float val) {
	if (val != 0) {
		if (!bInInspect) {
			GetMovementComponent()->AddInputVector(GetCapsuleComponent()->GetRightVector()*
				val*GetWorld()->GetDeltaSeconds() * 100);
		}
		else {
			switch (CurrentInspectComponent->InspectType) {
			case EInspectType::ObjectToCam: {
				
				
					CurrentInspectComponent->Move(CameraComponent->GetRightVector()*val*50.f*
						GetWorld()->GetDeltaSeconds());

				
				
				break;
			}
			case EInspectType::CamToObject: {
				CurrentInspectComponent->Move(FVector(0, val*50.f*
					GetWorld()->GetDeltaSeconds(), 0));
				break;
			}
			}
		}
	}
}

void APlayerCharacter::LookUp(float val) {
	if (val != 0 && !bInInspect) {

		GetPC()->SetControlRotation(GetPC()->GetControlRotation() +
			FRotator(val, 0, 0)*GetWorld()->GetDeltaSeconds() * 20);
	}

}

void APlayerCharacter::LookRight(float val) {
	if (val != 0 && !bInInspect) {

		GetPC()->SetControlRotation(GetPC()->GetControlRotation() +
			FRotator(0, val, 0)*GetWorld()->GetDeltaSeconds() * 20);

	}

}

void APlayerCharacter::FindItems() {
	FindedItems.Empty();
	for (TObjectIterator<AActor> iter; iter; ++iter) {
		auto Comp = Cast<UInspectComponent>(iter->GetComponentByClass(UInspectComponent::StaticClass()));
		if (Comp == nullptr)continue;
		FVector CompLoc = Comp->InteractPoint->GetComponentLocation();
		FHitResult hit;
		FCollisionQueryParams cqp(TEXT("FindItemsTrace"), false, this);
		float dist = FVector::Dist(CameraComponent->GetComponentLocation(), CompLoc);
		float dot = FVector::DotProduct(CameraComponent->GetForwardVector(),
			(CompLoc - CameraComponent->GetComponentLocation()) / dist);
		if (dist < 200.f &&
			dot > 0.9f &&
			GetWorld()->LineTraceSingleByChannel(hit,
				CameraComponent->GetComponentLocation(),
				CameraComponent->GetComponentLocation() +
				(CompLoc - CameraComponent->GetComponentLocation())
				* 200, ECC_GameTraceChannel3,
				cqp)
			) {
			if (hit.GetActor()->GetUniqueID() == iter->GetUniqueID())

				FindedItems.Add(TPairInitializer<float, UInspectComponent*>(dot, Comp));
		}

	}

}



void APlayerCharacter::TouchPressed(ETouchIndex::Type finger, FVector pos) {



	FVector2D size;
	GEngine->GameViewport->GetViewportSize(size);
	if (Touches.Num() == 0) {
			mLastX = pos.X;
			mLastY = pos.Y;
	}
	else if(Touches.Num()>0){
		float mX1 = 0.f, mY1 = 0;
		bool isPressed = false;
		
			GetPC()->GetInputTouchState(Touches[0], mX1, mY1, isPressed);
			LastDistZoom = FVector2D::Distance(FVector2D(mX1, mY1),FVector2D( pos));
	}

		Touches.Add(finger);


	
	





}

void APlayerCharacter::TouchReleased(ETouchIndex::Type finger, FVector pos) {
	FVector2D size;
	GEngine->GameViewport->GetViewportSize(size);
	Touches.RemoveSingleSwap(finger, true);
	if (Touches.Num() > 0) {
		float mX1 = 0.f, mY1 = 0;
		bool isPressed = false;

		GetPC()->GetInputTouchState(Touches[0], mX1, mY1, isPressed);
		mLastX = mX1;
		mLastY = mY1;
	}
	

	
}

void APlayerCharacter::TouchDoublePressed(ETouchIndex::Type finger, FVector pos) {
	if (finger == ETouchIndex::Touch1) {
		if (bInInspect) {
			CurrentInspectComponent->DeInspect();
		}
		else {
			if (FindedItems.Num() > 0) {
				FindedItems.Sort([](const TPair<float, UInspectComponent*>& a,
					const TPair<float, UInspectComponent*>& b) {
					return a.Key > b.Key; });
				if (FindedItems[0].Value->IsValidLowLevel()) {
					CurrentInspectComponent = FindedItems[0].Value;
					FindedItems[0].Value->Inspect(this);
					
				}
			}
		}
	}

}

void APlayerCharacter::OnMouseWheelUp(float val) {
	if (val != 0 && bInInspect) {
		switch (CurrentInspectComponent->InspectType) {
		case EInspectType::ObjectToCam: {
			CurrentInspectComponent->Move(
				CameraComponent->GetForwardVector() * -val*100.f*
				GetWorld()->GetDeltaSeconds());
			break;
		}
		case EInspectType::CamToObject: {
			CurrentInspectComponent->Move(FVector(val*50.f*
				GetWorld()->GetDeltaSeconds(), 0, 0));
			break;
		}
		}

	}
}



