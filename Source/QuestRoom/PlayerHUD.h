// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class QUESTROOM_API APlayerHUD : public AHUD
{
	GENERATED_BODY()
public:

	APlayerHUD();
	void DrawHUD()override;
	void BeginPlay() override;
private:
	UTexture2D* cross;
	TWeakObjectPtr<class APlayerCharacter> character;
	
};
