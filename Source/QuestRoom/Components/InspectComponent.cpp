// Fill out your copyright notice in the Description page of Project Settings.

#include "QuestRoom.h"
#include "InspectComponent.h"
#include "PlayerCharacter.h"


UInspectComponent::UInspectComponent() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
	InteractPoint = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractPoint"));
	InteractPoint->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);
	InteractPoint->SetCollisionProfileName("InteractPoint");
	InteractPoint->SetEnableGravity(false);
	InteractPoint->SetBoxExtent(FVector(1.f, 1.f, 1.f));
	

}



void UInspectComponent::BeginPlay() {
	Super::BeginPlay();


}


void UInspectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (CurrentInspector) {
		switch (InspectType) {
		case EInspectType::ObjectToCam: {
			if (bInterpLoc) {
				GetAttachmentRootActor()->SetActorLocation(
					FMath::VInterpTo(GetAttachmentRootActor()->GetActorLocation(),
						InterpLoc, DeltaTime, 40.f));
				if (GetAttachmentRootActor()->GetActorLocation().Equals(InterpLoc, 5.f*DeltaTime)) {
					bInterpLoc = false;
					if (!bInterpRot && !bInterpLoc && !bReadyForInteract) {
						bReadyForInteract = true;
						
					} else if (bReadyForInteract) {
						bReadyForInteract = false;
						
					}
				}
			}
			if (bInterpRot) {
				GetAttachmentRootActor()->SetActorRotation(
					FMath::RInterpTo(GetAttachmentRootActor()->GetActorRotation(),
						InterpRot, DeltaTime, 40.f));
				if (GetAttachmentRootActor()->GetActorRotation().Equals(InterpRot, 20.f*DeltaTime)) {
					bInterpRot = false;
					if (!bInterpRot && !bInterpLoc && !bReadyForInteract) {
						
						bReadyForInteract = true;
						
					} else if (bReadyForInteract) {
						bReadyForInteract = false;
						
					}
				}
			}
			break;
		}
		case EInspectType::CamToObject: {
			auto cam = CurrentInspector->CameraComponent;
			if (bInterpLoc) {

				cam->SetWorldLocation(FMath::VInterpTo(cam->GetComponentLocation(),
					InterpLoc, DeltaTime, 40.f));
				if (cam->GetComponentLocation().Equals(InterpLoc, 5.f*DeltaTime)) {
					bInterpLoc = false;
					if (!bInterpRot && !bInterpLoc && !bReadyForInteract) {
						bReadyForInteract = true;
					} else if (bReadyForInteract) {
						bReadyForInteract = false;
					}
				}
			}
			if (bInterpRot) {
				CurrentInspector->GetPC()->SetControlRotation(
					FMath::RInterpTo(cam->GetComponentRotation(),
						InterpRot, DeltaTime, 40.f));
				if (cam->GetComponentRotation().Equals(InterpRot, 20.f*DeltaTime)) {
					bInterpRot = false;
					if (!bInterpRot && !bInterpLoc && !bReadyForInteract) {
						bReadyForInteract = true;

					} else if (bReadyForInteract) {
						bReadyForInteract = false;
					}
				}
			}
			break;
		}
		}
	}

}

void UInspectComponent::Inspect(APlayerCharacter * Inspector) {
	auto pc = Inspector->GetPC();
	CurrentInspector = Inspector;
	Inspector->bInInspect = true;

	//pc->bShowMouseCursor = true;
	auto cam = Inspector->CameraComponent;
	switch (InspectType) {
	case EInspectType::ObjectToCam: {
		//pc->bEnableClickEvents = true;




		BeginLoc = GetAttachmentRootActor()->GetActorLocation();
		BeginRot = GetAttachmentRootActor()->GetActorRotation();
		InterpLoc = cam->GetComponentLocation() +
			cam->GetForwardVector()*50.f;
		InterpRot = FQuat(Inspector->CameraComponent->GetRightVector(), 0).Rotator();
		bInterpLoc = bInterpRot = true;

		break;
	}
	case EInspectType::CamToObject: {


		BeginLoc = cam->GetComponentLocation();
		BeginRot = CurrentInspector->GetPC()->GetControlRotation();
		FTransform n = FTransform(CamRotOffset, CamOffset)*GetAttachmentRootActor()->GetActorTransform();

		InterpLoc = n.GetLocation(); //GetAttachmentRootActor()->GetActorLocation()+CamOffset;
		InterpRot = n.GetRotation().Rotator();//InteractPoint->GetComponentRotation();
		bInterpLoc = bInterpRot = true;
		break;
	}
	}
	UE_LOG(LogTemp, Warning, TEXT("Inspect"));
}

void UInspectComponent::DeInspect() {
	if (!CurrentInspector->IsValidLowLevel())return;
	auto pc = CurrentInspector->GetPC();
	CurrentInspector->bInInspect = false;

	//pc->bShowMouseCursor = false;
	//pc->bEnableClickEvents = false;
	
	//FInputModeGameOnly s;
	//s.SetConsumeCaptureMouseDown(false);
	//pc->SetInputMode(s);

	bInterpLoc = bInterpRot = true;

	InterpLoc = BeginLoc;
	InterpRot = BeginRot;
	UE_LOG(LogTemp, Warning, TEXT("DeInspect"));
}

void UInspectComponent::Rotate(const FQuat& rot) {
	switch (InspectType) {
	case EInspectType::ObjectToCam: {
		//GetAttachmentRootActor()->SetActorTransform(FTransform(rot,FVector::ZeroVector)*
		//	GetAttachmentRootActor()->GetActorTransform());
		
		GetAttachmentRootActor()->AddActorWorldRotation(rot);
		break;
	}
	case EInspectType::CamToObject: {
		break;
	}
	}
	
}

void UInspectComponent::Move(const FVector & delta) {
	/*
	switch (InspectType) {
	case EInspectType::ObjectToCam: {
		break;
	}
	case EInspectType::CamToObject: {
		break;
	}
	}
	*/
	switch (InspectType) {
	case EInspectType::ObjectToCam: {
		auto cam = CurrentInspector->CameraComponent;
		float dist = FVector::Dist(cam->GetComponentLocation(),
			GetAttachmentRootActor()->GetActorLocation() + delta);
		float dot = FVector::DotProduct(cam->GetForwardVector(),
			(GetAttachmentRootActor()->GetActorLocation() + delta -
				cam->GetComponentLocation()) / dist);
		if (dot > 0.8f)
			GetAttachmentRootActor()->AddActorWorldOffset(delta);
		break;
	}
	case EInspectType::CamToObject: {
		auto cam = CurrentInspector->CameraComponent;
		cam->SetWorldTransform(FTransform(FRotator(0, 0, 0),delta)*
			cam->GetComponentTransform());

		break;
	}
	}
	
}


