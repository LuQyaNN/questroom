// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "InspectComponent.generated.h"

UENUM(BlueprintType)
enum class EInspectType:uint8
{ObjectToCam,CamToObject};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class QUESTROOM_API UInspectComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInspectComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(EditAnywhere)
		UBoxComponent* InteractPoint;
		
	UPROPERTY(EditAnywhere)
	FVector CamOffset;
	
	UPROPERTY(EditAnywhere)
	FRotator CamRotOffset;

	void Inspect(class APlayerCharacter* Inspector);

	void DeInspect();

	void Rotate(const FQuat& rot);

	void Move(const FVector& delta);
	
	bool bInterpLoc = false, bInterpRot = false;

	//true after interpolate to cam
	bool bReadyForInteract = false;

	UPROPERTY(EditAnywhere)
	EInspectType InspectType;
	
	FVector InterpLoc,BeginLoc;
	FRotator InterpRot,BeginRot;

	class APlayerCharacter* CurrentInspector = nullptr;

};
