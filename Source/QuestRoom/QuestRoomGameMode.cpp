// Fill out your copyright notice in the Description page of Project Settings.

#include "QuestRoom.h"
#include "PlayerCharacter.h"
#include "PlayerHUD.h"
#include "QuestRoomGameMode.h"


AQuestRoomGameMode::AQuestRoomGameMode()
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
	HUDClass = APlayerHUD::StaticClass();
}

